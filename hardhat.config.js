require("@nomiclabs/hardhat-waffle")

module.exports = {
  solidity: "0.8.4",

  paths: {
    artifacts: "./src/back/artifacts",
    sources: "./src/back/contracts",
    cache: "./src/back/cache",
    test: "./src/back/test"
  },

  networks: {
    hardhat: {},
    testnet: {
      url: "https://speedy-nodes-nyc.moralis.io/d62d14d8b5a461512153716c/bsc/testnet",
      accounts: ["0xad29ab8742bba4bec35a1c73f1c45b7020d984cb9018d54065049e1c473379a5"]
    }
  }
};
