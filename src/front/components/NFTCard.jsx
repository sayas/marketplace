import { ethers } from "ethers"

const NFTCard = ({item, onBuyItem}) => {

    return (
        <div className="card">
            <img className="card-img-top" src={item.image} alt="nft image"/>
            <div className="card-body">
                <h4 className="card-title text-center">{item.name}</h4>
                <p className="card-text text-center" style={{color: "#aaaaaa"}}>{item.description}</p>
                <h5 className="text-center small">
                    <img src={process.env.PUBLIC_URL + "/images/bnb.png"} width={20} className="mx-2"/>
                    {ethers.utils.formatEther(item.price)} 
                </h5>
                <center>
                    <a href="#" className="btn btn-outline-primary btn-block" onClick={onBuyItem}>Buy</a>
                </center>
            </div>
        </div>
    )
}

export default NFTCard