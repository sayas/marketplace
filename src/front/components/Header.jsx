import { Link } from "react-router-dom"

import "../styles/Header.css"

const Header = ({web3Handler, account}) => {
    return (
        <div className="navbar navbar-expand-sm navbar-light" id="header">
            <div className="container">
                <a className="navbar-brand" href="#">NFTMarketplace</a>
                
                {/* <input type="text" className="form-control mx-5" placeholder="Search items, collections, and accounts"/> */}

                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link className="nav-link" to={"/"}>Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to={"/create"}>Create</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to={"#"}>Items</Link>
                    </li>
                    <li className="nav-item">
                        { account ? (
                            <Link className="nav-link" to="#">{account.slice(0, 5) + '...' + account.slice(account.length - 4, account.length)}</Link>
                        ) : (
                            <a href="#" className="nav-link" onClick={web3Handler}>Connect</a>
                        )}
                        
                    </li>
                    
                </ul>
            </div>
        </div>
    )
}

export default Header