import { ethers } from "ethers"
import { create } from "ipfs-http-client"
import { useEffect, useState } from "react"

import "../styles/Create.css"

const ipfs = create("https://ipfs.infura.io:5001/api/v0")
const ipfsURL = "https://ipfs.infura.io/ipfs"

const Create = ({ marketplace, nft }) => {    
    const [selectedFile, setSelectedFile] = useState()
    const [preview, setPreview] = useState()

    const [image, setImage] = useState()
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState(0)

    const chooseImage = async (e) => {
        const files = e.target.files
        
        if(files.length != 0) {
            setSelectedFile(files[0])

            const added = await ipfs.add(files[0])
            const uri = `${ipfsURL}/${added.path}`
            
            setImage(uri)
        }
    }

    const createNFT = async () => {
        if(!name || !description || !price) return

        console.log("create nft...")

        try {
            const metadata = await ipfs.add(JSON.stringify({name, description, price, image})) 
            console.log(metadata)
            mint(metadata)
        } catch(error) {
            console.log(error)
        }
    }

    const mint = async (metadata) => {
        const uri = `${ipfsURL}/${metadata.path}`
    
        console.log(metadata)
        console.log(uri)

        await(await nft.mint(uri)).wait()

        const token = await nft.mint(uri)
        await token.wait()
    
        const tokentID = await nft.tokenCount()
        const approval = await nft.setApprovalForAll(marketplace.address, true)
        await approval.wait()

        const item = await marketplace.createItem(nft.address, tokentID, ethers.utils.parseEther(price.toString()))
        await item.wait()
    }

    useEffect(() => {
        if(!selectedFile) {
            setPreview(undefined)
            return
        }

        const previewURL = URL.createObjectURL(selectedFile)
        setPreview(previewURL)

        return () => URL.revokeObjectURL(preview)
    }, [selectedFile])

    return (
        <div id="create">
            <div className="container">
                <h3 className="page-title">Create new item</h3>

                <form>
                    <img width={200} src={preview}/>
                    
                    <div className="form-group">
                        <label>Image</label>
                        <input type="file" id="file" className="form-control" onChange={chooseImage} />
                    </div>

                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control" onChange={(e) => setName(e.target.value)} />
                    </div>
                    
                    <div className="form-group">
                        <label>Description</label>
                        <textarea className="form-control" rows="4" onChange={(e) => setDescription(e.target.value)}></textarea>
                    </div>
                    
                    <div className="form-group">
                        <label>Price</label>
                        <input type="number" className="form-control" onChange={(e) => setPrice(e.target.value)} />
                    </div>

                    <button className="btn btn-primary" onClick={createNFT}>Create & List NFT</button>
                </form>
            </div>
        </div>
    )
}

export default Create