import { useEffect, useState } from "react"

import NFTCard from "../components/NFTCard"

const Home = ({ marketplace, nft }) => {
    const [items, setItems] = useState([])

    const loadItems = async () => {
        const itemCount = await marketplace.itemCount()
        let items = []

        for(let i = 1; i <= itemCount; i++) {
            const item = await marketplace.items(i)
            
            if(!item.sold) {
                const uri = await nft.tokenURI(item.tokenId)
                const ipfsData = await fetch(uri)
                const metadata = await ipfsData.json()
                const price = await marketplace.getItemPrice(item.itemId)

                items.push({
                    id: item.itemId,
                    seller: item.seller,
                    name: metadata.name,
                    description: metadata.description,
                    image: metadata.image,
                    price: price,
                    sold: item.sold
                })
            }
        }

        setItems(items)
    }

    const buyItem = async (item) => {
        const purchase = await marketplace.purchaseItem(item.id, {value: item.price})
        await purchase.wait()
        loadItems()
    }

    useEffect(() => {
        loadItems()
    }, [])

    return (
        <div id="home">
            <div className="container">
                <h3 className="page-title">Explore NFTs</h3>

                <div className="row">
                    {items.length > 0 ? (
                        <>
                            {items.map((item, index) => (
                                <div key={index} className="col-md-3 my-2">
                                    <NFTCard item={item} onBuyItem={() => buyItem(item)} />
                                </div>
                            ))}
                        </>
                    ) : (
                        <div>
                            <p>Marketplace empty</p>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}

export default Home