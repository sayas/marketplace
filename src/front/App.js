
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useState } from 'react';
import { ethers } from 'ethers';

import Header from './components/Header';
import Home from './pages/Home';
import Create from './pages/Create';

import MarketplaceContract from "./contracts/Marketplace.json"
import NFTContract from "./contracts/NFT.json"

import './styles/App.css';
import 'bootstrap/dist/css/bootstrap.css'

function App() {
  const [connected, setConnected] = useState(false)

  const [account, setAccount] = useState(null)
  const [marketplace, setMarketplace] = useState({})
  const [nft, setNft] = useState({})

  const web3Handler = async () => {
    if (! window.ethereum) {
      alert('MetaMask not detected. Please try again from a MetaMask enabled browser.')
      return;
    }

    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    setAccount(accounts[0])

    const provider = new ethers.providers.Web3Provider(window.ethereum)
  
    const signer = provider.getSigner()

    window.ethereum.on('chainChanged', (chainId) => {
      window.location.reload();
    })

    window.ethereum.on('accountsChanged', async function (accounts) {
      setAccount(accounts[0])
      await web3Handler()
    })

    loadContract(signer)
  }

  const loadContract = async (signer) => {
    const marketplace = new ethers.Contract(MarketplaceContract.address, MarketplaceContract.artifacts.abi, signer)
    setMarketplace(marketplace)

    const nft = new ethers.Contract(NFTContract.address, NFTContract.artifacts.abi, signer)
    setNft(nft)

    setConnected(true)
  }

  return (
    <BrowserRouter>
      <div className="App">
        <>
          <Header web3Handler={web3Handler} account={account}/>
        </>
        <div className="p-4">
          {!connected ? (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '80vh' }}>
              <div className='text-center'>
                <h4>Get started</h4>
                <p className=''>You need to connect your metamask account. <a href='#' onClick={web3Handler}>connect</a></p>
              </div>
            </div>
          ) : (
          <Routes>
            <Route path='/' element={
              <Home marketplace={marketplace} nft={nft} />
            } />
            <Route path='/create' element={
              <Create marketplace={marketplace} nft={nft} />
            } />
          </Routes>
          )}
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
