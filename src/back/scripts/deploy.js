async function main() {
    const [deployer] = await ethers.getSigners();

    console.log("Deploying contracts with the account : ", deployer.address)
    console.log("Account balance : ", (await deployer.getBalance()).toString())

    const MarketplaceContract = await ethers.getContractFactory("Marketplace")
    const NFTContract = await ethers.getContractFactory("NFT")

    const Marketplace = await MarketplaceContract.deploy(1)
    const NFT = await NFTContract.deploy()

    saveContractInfo(Marketplace, "Marketplace")
    saveContractInfo(NFT, "NFT")

    console.log("Marketplace depoyed to ", Marketplace.address)
    console.log("NFT deployed to ", NFT.address)
}

function saveContractInfo(contract, name) {
    const fs = require("fs")
    const directory = __dirname + "/../../front/contracts"
    
    if (!fs.existsSync(directory)) {
        fs.mkdirSync(directory)
    }

    const contractData = {
        address: contract.address,
        artifacts: artifacts.readArtifactSync(name)
    }

    fs.writeFileSync(directory + `/${name}.json`, JSON.stringify(contractData, null, 2))
}

main()
.then(() => process.exit(0))
.catch((error) => {
    console.log(error)
    process.exit(1)
})
